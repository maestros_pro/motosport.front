import $ from 'jquery'
import Popup from '../modules/module.popup'
import PageNav from '../modules/module.pagenav'
import Form from '../modules/module.validate'
import ViewPort from '../modules/module.viewport'
import Inputmask from 'inputmask'
import 'slick-carousel'


import 'fullpage.js/dist/jquery.fullpage';
require('malihu-custom-scrollbar-plugin')($);


let screens = ['welcome','platform','programm','license','command','feedback','vehicle','contact'];

$(function () {


	let $b = $('body'),
		$footer = $('.footer'),
		activeScreen = null,
		$sectionTitle = $('.header__part'),
		$footerCounter = $('.footer__counter-current'),
		fp = null,
		fpOptions = {
			// scrollBar: true,
			menu: '.nav__menu-list',
			onLeave: (origin, destination, direction)=>{
				$('.nav__menu-link').removeClass('is-active');
				$('.nav__menu-list').find('li').eq(destination - 1).find('.nav__menu-link').addClass('is-active');
				pageNavAction($('.nav__menu-list li').eq(destination - 1).find('.nav__menu-link'));
			}
		}
	;

	window.app = window.app || {};

	window.app.popup = new Popup({
		onPopupOpen:(popup)=>{
			initGallerySlick($(popup).find('.slider_gallery'));
			if (fp) $.fn.fullpage.setAllowScrolling(false);
		},
		onPopupClose: ()=>{
			if (fp) $.fn.fullpage.setAllowScrolling(true)
		}
	});


	const form = new Form();

	new PageNav({
		heightElements: [],
		slideTime: 0,
		activeClass: 'is-active',
		menu: '.nav__menu-list',
		offset: 200,
		onChange:(e)=>{
			pageNavAction(e);
		}
	});


	function pageNavAction(e){

		// console.info(e);

		let name = e.attr('data-part'),
			title = e.attr('data-title'),
			count = 1,
			isLast = false,
			isFirst = false
		;
		activeScreen = name;

		if (name === '#welcome' && $footer.hasClass('is-light')){
			$footer.removeClass('is-light');
		} else if (name !== '#welcome' && !$footer.hasClass('is-light')){
			$footer.addClass('is-light');
		}

		if ( title ){
			$sectionTitle.addClass('is-active').html(title);
		} else {
			$sectionTitle.removeClass('is-active').html('');
		}

		switch(name){
			case '#welcome':
				isFirst = true;
				isLast = false;
				break;
			case '#contact':
				isFirst = false;
				isLast = true;
				break;
			default:
				isFirst = false;
				isLast = false;
				break;
		}

		switch(name){
			case '#welcome':
				count = 1;
				break;
			case '#platform':
				count = 2;
				break;
			case '#programm':
				count = 3;
				break;
			case '#license':
				count = 4;
				break;
			case '#command':
				count = 5;
				break;
			case '#feedback':
				count = 6;
				break;
			case '#vehicle':
				count = 7;
				break;
			case '#contact':
				count = 8;
				break;
			default:
				count = 0;
				break;
		}

		$footerCounter.html(count);

		$footer.find('.footer__control-item').removeClass('is-hide');

		if ( isFirst ){
			$footer.find('.footer__control-item_prev').addClass('is-hide');
		} else if ( isLast ){
			$footer.find('.footer__control-item_next').addClass('is-hide');
		}
	}

	initCardsSlick();
	initTabsSlick();
	initMapSlick();



	new ViewPort({
		'0': ()=>{

			if ( fp ) {
				$.fn.fullpage.destroy('all');
				fp = false;
			}

			initSingleSlick();
			initLicenseSlick();
			initPricesSlick(true);

			try {
				$('.js-scrollbar').mCustomScrollbar('destroy');
			} catch (e) {

			}

		},
		'1280': ()=>{

			fp = true;
			$('.website').fullpage(fpOptions);

			initSingleSlick(true);
			initLicenseSlick(true);
			initPricesSlick();
			$('.js-scrollbar').mCustomScrollbar();
		}
	});

	$('.command__tabs').mCustomScrollbar();
	$('.programm__tabs').mCustomScrollbar();
	$('.nav__inner').mCustomScrollbar();

	/*$('.js-scrollbar').each(function () {
		let $t = $(this);

		// if (!$t.closest('.slider').length){
			$t.mCustomScrollbar()
		// }
	});*/

	$b
		.on('click', (e)=>{
			if ( $('.header__contact-phones.is-open').size() ){
				if ( !$(e.target).closest('.header__contact-phones.is-open').size() ) $('.header__contact-phones.is-open').removeClass('is-open');
			}
			if ( $('.show-menu').size() ){
				if ( !$(e.target).closest('.header__nav').size() ) $b.removeClass('show-menu');
			}
		})

		.on('click', '.nav__handler', function(){
			$b.toggleClass('show-menu');
		})

		.on('click', '.nav__menu-link', function(){
			let $t = $(this),
				hash = $t.attr('data-part')
			;

			$b.removeClass('show-menu');


			if ( fp ){
				$.fn.fullpage.moveTo($t.closest('li').index() + 1);
			} else {
				$('html,body').animate({scrollTop:$(hash).offset().top + 1}, 0);
			}

		})

		.on('click', '.footer__control-item', function(){
			let $t = $(this),
				onPrev = $t.hasClass('footer__control-item_prev'),
				onNext = $t.hasClass('footer__control-item_next'),
				$el = null
			;

			if ( activeScreen && onPrev ){
				$el = $(activeScreen).prev();
			} else if ( activeScreen && onNext ){
				$el = $(activeScreen).next();
			}

			$('html,body').animate({scrollTop:$el.offset().top + 1}, 500);

		})

		.on('click', '.header__contact-phones-num', function(){
			$(this).closest('.header__contact-phones').toggleClass('is-open');
		})

		.on('click', '[data-goto]', function(){
			let $t = $(this),
				slideId = $t.attr('data-goto'),
				$slide = $('[data-slide-id="' + slideId + '"]'),
				$section = $slide.closest('.section'),
				index = $slide.closest('[data-slick-index]').attr('data-slick-index')
			;

			app.popup.close('.popup');

			if ( fp ){
				$.fn.fullpage.moveTo($('.nav__menu-link[data-part="#' + $section.attr('id') +'"]').closest('li').index() + 1);
			} else {
				$('html,body').animate({scrollTop:$section.offset().top + 1}, 500);
			}

			$slide.closest('.slider').slick('slickGoTo', index);

		})
	;



window.app.$ = $;




	function initSingleSlick(destroy) {

		$('.slider_sing').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider__control')
			;

			if ( destroy ){

				try{ $slider.slick('unslick');} catch (e){}

			} else {
				$slider
					.on('init', function (event, slick) {

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.slick({
						dots: false,
						infinite: false,
						speed: 300,
						swipe: true,
						swipeToSlide: true,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						adaptiveHeight: false,
						prevArrow: $control.find('.slider__control-prev'),
						nextArrow: $control.find('.slider__control-next')
					});
			}

		});

	}

	function initMapSlick() {

		$('.slider_map').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider__control')
			;

			$slider
				.on('init', function (event, slick) {

				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

				})
				.slick({
					dots: false,
					infinite: false,
					speed: 300,
					swipe: true,
					swipeToSlide: true,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $control.find('.slider__control-prev'),
					nextArrow: $control.find('.slider__control-next')
				});

		});

	}


	function initTabsSlick() {

		$('.slider_tab').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider__control'),
				$tabLink = $wrap.find('[data-tab-slide]')
			;

			$slider
				.on('init', (event, slick)=>{

				})
				.on('beforeChange', (event, slick, currentSlide, nextSlide)=>{

					if ( slick.$slider.hasClass('slider_tab') ){
						$tabLink
							.removeClass('is-active')
							.filter('[data-tab-slide=' + nextSlide + ']')
							.addClass('is-active');
					}

				})
				.slick({
					dots: false,
					infinite: false,
					speed: 300,
					swipe: true,
					swipeToSlide: false,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: false,
					prevArrow: $control.find('.slider__control-prev'),
					nextArrow: $control.find('.slider__control-next'),
					mobileFirst: true,
					responsive: [
						{
							breakpoint: 1279,
							settings: {
								fade: true,
							}
						}
					]

				});


			$tabLink.on('click', function () {
				let $t = $(this),
					slide = $t.attr('data-tab-slide')
				;
				$slider.slick('slickGoTo', slide);
			});

		});

	}


	function initLicenseSlick(destroy) {

		$('.slider_lisence').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider__control')
			;


			if ( destroy ){

				try{ $slider.slick('unslick');} catch (e){}

			} else {
				$slider
					.on('init', function (event, slick) {

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.slick({
						dots: false,
						infinite: false,
						speed: 300,
						swipe: true,
						swipeToSlide: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						adaptiveHeight: false,
						prevArrow: $control.find('.slider__control-prev'),
						nextArrow: $control.find('.slider__control-next'),
						variableWidth: true
					});
			}

		});

	}

	function initCardsSlick() {

		$('.slider_cards').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider__control')
			;

			$slider
				.on('init', function (event, slick) {

				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

				})
				.slick({
					dots: false,
					infinite: false,
					speed: 300,
					swipe: true,
					swipeToSlide: false,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: false,
					prevArrow: $control.find('.slider__control-prev'),
					nextArrow: $control.find('.slider__control-next'),
					variableWidth: true
				});

		});

	}

	function initPricesSlick(destroy) {

		$('.slider_prices').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent()
			;


			if ( destroy ){

				try{ $slider.slick('unslick');} catch (e){}

			} else {
				$slider
					.on('init', function (event, slick) {

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.slick({
						dots: false,
						infinite: false,
						speed: 300,
						swipe: false,
						swipeToSlide: false,
						arrows: true,
						slidesToShow: 4,
						slidesToScroll: 1,
						adaptiveHeight: false,
						prevArrow: $wrap.find('.slider_prices-control-prev'),
						nextArrow: $wrap.find('.slider_prices-control-next'),
						variableWidth: false
					});
			}

		});

	}

	function initGallerySlick($el) {

		$el.each(function () {

			let $slider = $(this),
				$wrap = $slider.parent()
			;

			if ( $slider.hasClass('is-slider-inited') ) {
				setTimeout(()=>{ $(window).trigger('resize'); }, 10);
				return false;
			}
			$slider.addClass('is-slider-inited');

			$slider
				.on('init', function (event, slick) {
					// console.info(slick);

					if (!!$slider.closest('.popup_detail').length) slick.$dots.find('button').html('<svg xmlns="http://www.w3.org/2000/svg" width="220" height="220" viewBox="0 0 220 220"><path d="M189.792,143.57l0.313,3.752C189.164,146.7,189.164,145.442,189.792,143.57ZM35.769,139.818a1.091,1.091,0,0,1,.314.627Zm67.889-78.5v2.812H98.645c0,3.448-.942,5.319-2.815,5.319,0,2.185-3.129,5.31-9.7,9.689v1.254c0,1.832,1.256,2.811,4.071,2.811v1.273l-2.815,2.812,1.256,3.125-22.5-12.794-5.944-8.131A63.3,63.3,0,0,1,99.322,46.959L101.47,51.6c-0.942.588-1.569,0.94-1.569,1.567l2.5,0.627,1.256,2.812v2.5c-1.256.353-2.188,0.666-3.757,0.98v1.234h3.757ZM169.1,133.284l-10.957-1.254-3.443-2.184,0.314-.314-2.815-2.811v1.567l-21.286-12.2a23.154,23.154,0,0,1-6.866,10.315l7.514,4.379h-2.816v3.752h0.628l1.874-2.5v-1.254l20.658,11.883,6.572,8.758a63.251,63.251,0,0,1-39.433,21.836l-4.071-9.688V131.061a24.31,24.31,0,0,1-5.012.626,18.968,18.968,0,0,1-7.514-1.253V163.6l-4.071,9.375a63.968,63.968,0,0,1-38.8-23.159l4.071-5.319,1.569,1.567v1.568l0.255-.255h-0.02v-0.627h1.57l2.5-2.5-3.757-2.811L80.479,133l0.314,0.314v1.567l0.981-.98h1.569l1.57-1.567,2.187-3.125,7.2-4.065A22.008,22.008,0,0,1,88.355,114.2l-6.278,3.732v-6.25H79.252v4.066l2.815,3.125-8.455,4.692a1.092,1.092,0,0,1-.628.314l-13.144,7.5-9.7,1.254a11.2,11.2,0,0,1-1.255-3.439,16.787,16.787,0,0,0,5.944-1.254L56.4,124.82h1.569l4.071-4.065v-1.568l5.326,2.185,3.168-12.451-2.187-2.185a7.1,7.1,0,0,1-7.2.98l-4.071,5.623a28.45,28.45,0,0,1-10.329,5.623v2.5a60.971,60.971,0,0,1-.981-10.629,63.347,63.347,0,0,1,4.385-23.139l10.015,1.254,28.172,16.262a20.284,20.284,0,0,1,6.258-10.942l-2.5-1.254,0.981-.98V90.778l-1.256-1.254c2.521,0,5.631-3.135,9.387-9.071l-0.314-.313L99.636,76.7h1.255a11.921,11.921,0,0,0,3.129-.98V89.054a21.376,21.376,0,0,1,12.527,0V57.167l4.071-9.689A63.3,63.3,0,0,1,159.746,70.01l-6.572,8.758-27.24,15.674a19.841,19.841,0,0,1,5.944,10.942l27.574-15.86,6.866-.98,1.874,1.871,1.547-1.545a64.974,64.974,0,0,1,3.75,21.274A63.325,63.325,0,0,1,169.1,133.284Zm-102.4,14.1H65.553l2.792,7.132v1.254l-1.275,1.244h1.275l1.157,1.313c1.883,0,3.09-2.812,4.071-8.131C73.573,148.361,71.405,147.381,66.707,147.381Zm11.016-2.831H76.467v4.066l2.815,2.811,2.511-.02C80.851,146.735,79.292,144.55,77.722,144.55ZM103.658,129.5l3.8-.049-2.816-2.812-0.98.98V129.5Zm6.258-32.837a12.823,12.823,0,1,0,0,25.646h0.049A12.823,12.823,0,0,0,109.916,96.666ZM147.8,64.2l0.981-.98-2.816-2.811h-3.757l-2.256,1.92C144.042,64.828,146.543,65.455,147.8,64.2ZM159.442,114.5h-2.815v4.065l2.746,2.773h4.071l1.629-1.519Zm10-26.577,0.627,0.627-0.326.326c-0.109-.307-0.207-0.618-0.321-0.924Zm50.713,12.2v19.74l-9.7,3.125a97.393,97.393,0,0,1-2.816,13.714l7.828,6.887-2.815,6.858-0.628.627v0.627l-4.071,9.688-10.015-.979a72.714,72.714,0,0,1-5.326,8.13,15.34,15.34,0,0,0-4.071-2.5l-3.443.98-1.256-.98h-5.63l-1.256.98-2.187-5.623-9.956,6.025a77.213,77.213,0,0,0,17.529-24.7c0.313,0.617.313,1.244,0.627,1.871l5.945,5.937a13.223,13.223,0,0,0-1.57,2.184v-2.184h-2.187l-2.188,2.184,2.188,2.185h2.187v-0.314a9.16,9.16,0,0,0,6.258-3.125l0.628,0.627q4.227-6.093,2.815-7.5l-2.815,1.872V145.2l1.256-1.254h-1.256l-1.255-.98h-2.816a11.764,11.764,0,0,1,1.874-3.125l-0.981-.98-2.717,2.851-1.874-1.871,3.757-3.752c-2.5-2.811-3.129-4.683-1.873-5.937a8.019,8.019,0,0,1-.628-1.567,72.592,72.592,0,0,0,2.187-18.446A76.7,76.7,0,0,0,152.33,43.521V38.2l-0.98-.98h-1.874l-1.256,3.752a83.132,83.132,0,0,0-13.144-5.623L135.39,34.1l-2.815.627A89.113,89.113,0,0,0,110.014,31.6a77.9,77.9,0,0,0-34.126,7.5l-0.981-5.937h-2.5l-1.256.98a6.588,6.588,0,0,0,2.187,5.936A80.191,80.191,0,0,0,32.012,92.3H29.2l-4.071,4.056L29.2,97.606l1.854-.5a63.205,63.205,0,0,0-1.256,13.441,78.987,78.987,0,0,0,3.443,22.825l-0.981.98v1.568c1.609,1.567,1.609,3.085.628,4.065l-1.57,1.567-4.071.98L25.675,144.1l0.981,0.98c1.52,1.254,1.207,3.135-.981,5.319l2.187,2.185,4.071-2.5,0.981,0.98v1.567l-4.7,3.125,0.54,0.715,3.757,0.627c3.443-4.692,4.7-8.131,3.129-10.942l0.981-.98H37.6a97.608,97.608,0,0,0,8.455,13.441L42.929,160.8,44.5,162.37l2.5-2.5q1.99,2.521,4.179,4.877a79.715,79.715,0,0,0,112.551,4.194v2.811c2.187,0,4.061,1.568,5.63,4.379l6.572-1.254c2.2,0.353,3.757.667,5.327,0.98,0.353,0.931.667,1.519,0.981,2.5l0.98-.979,3.13,6.25,4.384-6.564,1.481,0.49v1.254c-2.187,0-3.443.617-3.443,2.184h1.256a10.369,10.369,0,0,0,4.7-1.254l0.313,0.627L181.307,194.08l-3.757-1.871,2.187-2.184h-3.129l-0.981.979,0.981,0.98-4.385-2.185a93.9,93.9,0,0,1-11.584,7.837l0.892,10.022-18.156,7.5-6.572-7.5a97.666,97.666,0,0,1-13.733,2.811l-3.129,9.689H100.215l-3.13-9.689a97.633,97.633,0,0,1-13.733-2.811l-6.572,7.5-18.157-7.5,0.912-9.983c-4.071-2.517-7.828-5.025-11.585-7.837l-9.083,4.693L25.136,180.816l1.874-3.752,1.825,1.793,1.569-1.568-2.187-2.184-0.314.362,1.57-3.125a44.752,44.752,0,0,1-3.443-4.692c3.129-5.937,4.071-9.062,2.815-10.316l-0.628.627H26.7v-1.567L22,153.269l-1.569,1.871,3.129,6.25h1.569l-1.874,1.871c-0.588-.931-0.942-1.518-1.569-2.5l-10.015.98L4.154,143.61l1.256-1.254,2.5,0.313L9.784,138.6l2.187-1.871v-0.314l4.7-2.5,0.981-.98-2.187-2.184H13.845l-2.5,2.811c-0.618-3.438-1.56-7.19-1.874-10.629l-9.7-3.125v-19.7l9.083-2.861a97.361,97.361,0,0,1,2.815-13.715L4.154,76.975,11.687,58.8,21.7,59.783c2.521-4.065,5.032-7.817,7.847-11.569l-4.7-9.071L38.584,25.427l9.083,4.692a93.974,93.974,0,0,1,11.624-7.857l-0.981-10,18.157-7.5,6.621,7.494A97.733,97.733,0,0,1,96.821,9.439L99.95-.249h19.716l3.178,9.7a97.733,97.733,0,0,1,13.733,2.812l6.572-7.5,18.2,7.524-0.981,10c4.071,2.518,7.828,5.026,11.585,7.837l8.749-4.732L194.431,39.1l-4.7,9.071a93.9,93.9,0,0,1,7.847,11.569l10.015-.98,4.071,10-1.874,2.5,0.981,0.98h2.5l1.873,4.467-7.514,6.564a97.43,97.43,0,0,1,2.816,13.715ZM46.1,172.959h1.255v-4.065H44.538l-1.255,1.254Zm-5.3.627,0.981-.979v-1.568H38.653l-0.981.98-0.02,1.567H40.8ZM53.66,45.666H50.531l-0.981.98,3.129,3.125,2.55-2.537Zm3.119-12.51-0.981.98L57.368,35.7l2.541-2.547H56.78Zm10.613-1.2H66.138l2.188,2.185h1.255ZM99.322,12.28c-3.757,1.234-12.84,3.732-27.24,6.857v2.8a33.553,33.553,0,0,1,19.412,5.319V26l1.256,1.254h5.63l5.631-4.065-0.04.343h0.981Zm49.153,20.611h-3.443l2.188,2.185h1.255V32.892Zm5.3-11.3h-3.727l-0.628.98,2.815,2.812,1.874-1.871Zm51.361,126.676v2.175l1.256,1.254,3.443-1.254-0.314.009v-3.438Z"/></svg>')
				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

				})
				.slick({
					dots: !!$slider.closest('.popup_detail').length,
					infinite: false,
					speed: 300,
					swipe: true,
					//-appendDots: ,
					fade:true,
					swipeToSlide: true,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true,
					prevArrow: $wrap.find('.slider__control-prev'),
					nextArrow: $wrap.find('.slider__control-next'),
					variableWidth: false

				});

		});

	}

	initInnerSlick();

	function initInnerSlick() {

		$('.command__item-course-list, .programm__item-course-list').each(function () {

			let $slider = $(this),
				$sliderWrap = $slider.closest('.slider')
			;

			$slider
				.on('mousedown touchstart', function(){
					$sliderWrap.slick('slickSetOption', {swipe: false})
				})
				.on('init', function (event, slick) {
					//console.info(slick);

					if (!!$slider.closest('.popup_detail').length) slick.$dots.find('button').html('<svg xmlns="http://www.w3.org/2000/svg" width="220" height="220" viewBox="0 0 220 220"><path d="M189.792,143.57l0.313,3.752C189.164,146.7,189.164,145.442,189.792,143.57ZM35.769,139.818a1.091,1.091,0,0,1,.314.627Zm67.889-78.5v2.812H98.645c0,3.448-.942,5.319-2.815,5.319,0,2.185-3.129,5.31-9.7,9.689v1.254c0,1.832,1.256,2.811,4.071,2.811v1.273l-2.815,2.812,1.256,3.125-22.5-12.794-5.944-8.131A63.3,63.3,0,0,1,99.322,46.959L101.47,51.6c-0.942.588-1.569,0.94-1.569,1.567l2.5,0.627,1.256,2.812v2.5c-1.256.353-2.188,0.666-3.757,0.98v1.234h3.757ZM169.1,133.284l-10.957-1.254-3.443-2.184,0.314-.314-2.815-2.811v1.567l-21.286-12.2a23.154,23.154,0,0,1-6.866,10.315l7.514,4.379h-2.816v3.752h0.628l1.874-2.5v-1.254l20.658,11.883,6.572,8.758a63.251,63.251,0,0,1-39.433,21.836l-4.071-9.688V131.061a24.31,24.31,0,0,1-5.012.626,18.968,18.968,0,0,1-7.514-1.253V163.6l-4.071,9.375a63.968,63.968,0,0,1-38.8-23.159l4.071-5.319,1.569,1.567v1.568l0.255-.255h-0.02v-0.627h1.57l2.5-2.5-3.757-2.811L80.479,133l0.314,0.314v1.567l0.981-.98h1.569l1.57-1.567,2.187-3.125,7.2-4.065A22.008,22.008,0,0,1,88.355,114.2l-6.278,3.732v-6.25H79.252v4.066l2.815,3.125-8.455,4.692a1.092,1.092,0,0,1-.628.314l-13.144,7.5-9.7,1.254a11.2,11.2,0,0,1-1.255-3.439,16.787,16.787,0,0,0,5.944-1.254L56.4,124.82h1.569l4.071-4.065v-1.568l5.326,2.185,3.168-12.451-2.187-2.185a7.1,7.1,0,0,1-7.2.98l-4.071,5.623a28.45,28.45,0,0,1-10.329,5.623v2.5a60.971,60.971,0,0,1-.981-10.629,63.347,63.347,0,0,1,4.385-23.139l10.015,1.254,28.172,16.262a20.284,20.284,0,0,1,6.258-10.942l-2.5-1.254,0.981-.98V90.778l-1.256-1.254c2.521,0,5.631-3.135,9.387-9.071l-0.314-.313L99.636,76.7h1.255a11.921,11.921,0,0,0,3.129-.98V89.054a21.376,21.376,0,0,1,12.527,0V57.167l4.071-9.689A63.3,63.3,0,0,1,159.746,70.01l-6.572,8.758-27.24,15.674a19.841,19.841,0,0,1,5.944,10.942l27.574-15.86,6.866-.98,1.874,1.871,1.547-1.545a64.974,64.974,0,0,1,3.75,21.274A63.325,63.325,0,0,1,169.1,133.284Zm-102.4,14.1H65.553l2.792,7.132v1.254l-1.275,1.244h1.275l1.157,1.313c1.883,0,3.09-2.812,4.071-8.131C73.573,148.361,71.405,147.381,66.707,147.381Zm11.016-2.831H76.467v4.066l2.815,2.811,2.511-.02C80.851,146.735,79.292,144.55,77.722,144.55ZM103.658,129.5l3.8-.049-2.816-2.812-0.98.98V129.5Zm6.258-32.837a12.823,12.823,0,1,0,0,25.646h0.049A12.823,12.823,0,0,0,109.916,96.666ZM147.8,64.2l0.981-.98-2.816-2.811h-3.757l-2.256,1.92C144.042,64.828,146.543,65.455,147.8,64.2ZM159.442,114.5h-2.815v4.065l2.746,2.773h4.071l1.629-1.519Zm10-26.577,0.627,0.627-0.326.326c-0.109-.307-0.207-0.618-0.321-0.924Zm50.713,12.2v19.74l-9.7,3.125a97.393,97.393,0,0,1-2.816,13.714l7.828,6.887-2.815,6.858-0.628.627v0.627l-4.071,9.688-10.015-.979a72.714,72.714,0,0,1-5.326,8.13,15.34,15.34,0,0,0-4.071-2.5l-3.443.98-1.256-.98h-5.63l-1.256.98-2.187-5.623-9.956,6.025a77.213,77.213,0,0,0,17.529-24.7c0.313,0.617.313,1.244,0.627,1.871l5.945,5.937a13.223,13.223,0,0,0-1.57,2.184v-2.184h-2.187l-2.188,2.184,2.188,2.185h2.187v-0.314a9.16,9.16,0,0,0,6.258-3.125l0.628,0.627q4.227-6.093,2.815-7.5l-2.815,1.872V145.2l1.256-1.254h-1.256l-1.255-.98h-2.816a11.764,11.764,0,0,1,1.874-3.125l-0.981-.98-2.717,2.851-1.874-1.871,3.757-3.752c-2.5-2.811-3.129-4.683-1.873-5.937a8.019,8.019,0,0,1-.628-1.567,72.592,72.592,0,0,0,2.187-18.446A76.7,76.7,0,0,0,152.33,43.521V38.2l-0.98-.98h-1.874l-1.256,3.752a83.132,83.132,0,0,0-13.144-5.623L135.39,34.1l-2.815.627A89.113,89.113,0,0,0,110.014,31.6a77.9,77.9,0,0,0-34.126,7.5l-0.981-5.937h-2.5l-1.256.98a6.588,6.588,0,0,0,2.187,5.936A80.191,80.191,0,0,0,32.012,92.3H29.2l-4.071,4.056L29.2,97.606l1.854-.5a63.205,63.205,0,0,0-1.256,13.441,78.987,78.987,0,0,0,3.443,22.825l-0.981.98v1.568c1.609,1.567,1.609,3.085.628,4.065l-1.57,1.567-4.071.98L25.675,144.1l0.981,0.98c1.52,1.254,1.207,3.135-.981,5.319l2.187,2.185,4.071-2.5,0.981,0.98v1.567l-4.7,3.125,0.54,0.715,3.757,0.627c3.443-4.692,4.7-8.131,3.129-10.942l0.981-.98H37.6a97.608,97.608,0,0,0,8.455,13.441L42.929,160.8,44.5,162.37l2.5-2.5q1.99,2.521,4.179,4.877a79.715,79.715,0,0,0,112.551,4.194v2.811c2.187,0,4.061,1.568,5.63,4.379l6.572-1.254c2.2,0.353,3.757.667,5.327,0.98,0.353,0.931.667,1.519,0.981,2.5l0.98-.979,3.13,6.25,4.384-6.564,1.481,0.49v1.254c-2.187,0-3.443.617-3.443,2.184h1.256a10.369,10.369,0,0,0,4.7-1.254l0.313,0.627L181.307,194.08l-3.757-1.871,2.187-2.184h-3.129l-0.981.979,0.981,0.98-4.385-2.185a93.9,93.9,0,0,1-11.584,7.837l0.892,10.022-18.156,7.5-6.572-7.5a97.666,97.666,0,0,1-13.733,2.811l-3.129,9.689H100.215l-3.13-9.689a97.633,97.633,0,0,1-13.733-2.811l-6.572,7.5-18.157-7.5,0.912-9.983c-4.071-2.517-7.828-5.025-11.585-7.837l-9.083,4.693L25.136,180.816l1.874-3.752,1.825,1.793,1.569-1.568-2.187-2.184-0.314.362,1.57-3.125a44.752,44.752,0,0,1-3.443-4.692c3.129-5.937,4.071-9.062,2.815-10.316l-0.628.627H26.7v-1.567L22,153.269l-1.569,1.871,3.129,6.25h1.569l-1.874,1.871c-0.588-.931-0.942-1.518-1.569-2.5l-10.015.98L4.154,143.61l1.256-1.254,2.5,0.313L9.784,138.6l2.187-1.871v-0.314l4.7-2.5,0.981-.98-2.187-2.184H13.845l-2.5,2.811c-0.618-3.438-1.56-7.19-1.874-10.629l-9.7-3.125v-19.7l9.083-2.861a97.361,97.361,0,0,1,2.815-13.715L4.154,76.975,11.687,58.8,21.7,59.783c2.521-4.065,5.032-7.817,7.847-11.569l-4.7-9.071L38.584,25.427l9.083,4.692a93.974,93.974,0,0,1,11.624-7.857l-0.981-10,18.157-7.5,6.621,7.494A97.733,97.733,0,0,1,96.821,9.439L99.95-.249h19.716l3.178,9.7a97.733,97.733,0,0,1,13.733,2.812l6.572-7.5,18.2,7.524-0.981,10c4.071,2.518,7.828,5.026,11.585,7.837l8.749-4.732L194.431,39.1l-4.7,9.071a93.9,93.9,0,0,1,7.847,11.569l10.015-.98,4.071,10-1.874,2.5,0.981,0.98h2.5l1.873,4.467-7.514,6.564a97.43,97.43,0,0,1,2.816,13.715ZM46.1,172.959h1.255v-4.065H44.538l-1.255,1.254Zm-5.3.627,0.981-.979v-1.568H38.653l-0.981.98-0.02,1.567H40.8ZM53.66,45.666H50.531l-0.981.98,3.129,3.125,2.55-2.537Zm3.119-12.51-0.981.98L57.368,35.7l2.541-2.547H56.78Zm10.613-1.2H66.138l2.188,2.185h1.255ZM99.322,12.28c-3.757,1.234-12.84,3.732-27.24,6.857v2.8a33.553,33.553,0,0,1,19.412,5.319V26l1.256,1.254h5.63l5.631-4.065-0.04.343h0.981Zm49.153,20.611h-3.443l2.188,2.185h1.255V32.892Zm5.3-11.3h-3.727l-0.628.98,2.815,2.812,1.874-1.871Zm51.361,126.676v2.175l1.256,1.254,3.443-1.254-0.314.009v-3.438Z"/></svg>')
				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

				})
				.on('afterChange', function () {
					$sliderWrap.slick('slickSetOption', {swipe: true});
				})
				.slick({
					dots: !!$slider.closest('.popup_detail').length,
					infinite: false,
					speed: 300,
					swipe: true,
					swipeToSlide: true,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: false,
					prevArrow: '<svg class="slick-arrow_prev" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path d="M134.21,56.6l0.405-.4h0.507v0.4l-0.507.4Zm8.519-19.7,0.811-.2,0.406,0.6-0.2.6a1.741,1.741,0,0,1-1.014-.8V36.9Zm7.911,7.5a1.378,1.378,0,0,0,.507.9l-0.406-1.5a2.277,2.277,0,0,1-.912.1v0.4l-0.406-.4V43.7a1.688,1.688,0,0,0,1.116-.9l-0.2-.7c0-.1-0.1-0.2-0.1-0.3l-0.405-1.5a0.364,0.364,0,0,1-.1-0.3V39.8h-0.2c0,0.7-.2,1-0.608,1V39.4H148.51c0,0.5-.1.8-0.406,0.8a0.544,0.544,0,0,0,0,.151,0.537,0.537,0,0,0,.609.449l-0.406.4c-0.71,0-1.521-1-2.738-3.2l-0.812-1,0.406-.4a1.622,1.622,0,0,0-1.014-1.6l-0.811.4c0-.4-0.1-0.6-0.406-0.6l-0.811.2V34.8l0.2-1h-0.405v0.4h-0.406l-0.811-.6h-1.015v1.2a1.977,1.977,0,0,1,.62,0,1.923,1.923,0,0,1,1.612,2.2,0.866,0.866,0,0,0-1.015,1h-0.1l-1.014,3.8c0,0.1-.1.2-0.1,0.3l-1.521,6c-1.724,4.6-2.84,7.1-3.245,7.5l-1.42-.2-1.623.2-1.014-.2-1.217.2a0.657,0.657,0,0,0-.812-0.6l-1.217.4h-0.2c-0.3,0-.405-0.2-0.405-0.6l0.608-1.5,0.2,0.2h0.609V52.9h-0.406V52.7c0-.7.608-2.1,1.927-4l0.609-1.4h-0.406V46.8h0.1l0.305,0.3,2.941-6.9H133.2V39.4q1.218,0,1.218-.6l-0.406-1.2,0.406-.8a4.052,4.052,0,0,0-1.218-2.6l-0.2-2.4L133.6,30l-0.2-1.2,0.2-.6h0.406l0.2,0.8-0.1,1.2c0.1,1.2.406,1.8,0.812,1.8h1.42c0.3,0,.709-0.6,1.014-1.8l-0.2-.6,0.406-1.3-0.2-.8a1.093,1.093,0,0,1,.811-0.8l0.609,0.2,0.405-.6-0.2-.2h-1.217V25.7h0.2l0.609,0.2c1.014-1.6,2.028-2.5,3.144-2.6l2.738-.2,2.434,0.2,1.015-.2,0.811,0.8,0.2-.1V22.2h1.014v0.4l-0.406,1.2v0.4l0.2,0.6h-0.2l-0.608.2,3.144,7.7-0.2,1,0.2-.2c0.608,0,1.115,1,1.623,3.2a1.124,1.124,0,0,1-.305.8l0.305,0.2,0.2-.2h0.2l0.406,0.4v0.2h-0.2v0.2l-0.2.2c0-.1-0.1-0.1-0.1,0V37.9l-0.406.4v0.4h0.406l0.2-.2c0.405,0.3,1.724,1.9,2.332,1.8l2.942,6.8,0.2-.2h0.2v0.4h-0.405l0.71,1.4c1.217,2,1.825,3.4,1.825,4.1V53h-0.406v0.6H162.2l0.2-.2,0.608,1.4c0,0.4-.1.6-0.405,0.6h-0.2l-0.913-.3-0.406.3h-0.811v0.2l-1.116-.2-0.71.2-0.1-.1-0.3.1-0.812-.1-0.608.1-0.2-.2-1.217.4H154.19c-1.319-1.6-2.942-5.1-4.97-10.6A2.313,2.313,0,0,1,150.64,44.4Zm-20.893,8.4h-0.3l-0.609.6v0.4h0.913l0.2-.9-0.2-.2v0.1Zm0.406-1.5v1.2h0.406V52.3l-0.2-1h-0.2Zm4.26-11.7-0.7.516a1.842,1.842,0,0,0,.8-0.516h-0.1Zm1.521-1.5h-0.2v0.2l0.2,0.2c0-.1.1-0.1,0.1,0V38Zm2.637-2,1.318-.4,0.2,0.2V35.7l-0.913-.3A0.669,0.669,0,0,0,138.571,36.1ZM134.615,33l-0.1.2v1.2l0.406,0.4a1.089,1.089,0,0,0,.811-0.6V33.6C135.731,33.3,135.325,33.1,134.615,33Zm12.678,4.1V36.9l-0.811-.2-0.406.6,0.2,0.6A1.741,1.741,0,0,0,147.293,37.1Zm3.347,8.8a2.37,2.37,0,0,0,.608,1.4l-0.3,1.3h1.014a0.587,0.587,0,0,0-.1-0.4l-0.507-2.1a0.541,0.541,0,0,0-.3-0.2H150.64Zm8.012-3.9,0.2-.2,0.609,0.8-0.406.4-0.406-.4V42Zm2.739-4.5V37.1l-0.2-.2V36.7L162,36.1l-0.2-.6H162l0.2-.2h0.2l0.608,0.2v0.4a4.007,4.007,0,0,0-1.217,2l-0.608-.2Zm-6.9-17.8h0.2c0.3,0,.507.6,0.811,1.8H155.1l-1.622-.4Zm2.535,0.1,0.2-.2,0.2,0.6v0.2h-0.406l-0.2-.6h0.2ZM144.555,17h0.405v0.4l-0.811,1h-0.2V17.6Zm-3.246,4.4a1.043,1.043,0,0,1-.608-1.2h1.014l-0.2,1.2h-0.2ZM135.528,28H134.92V27.5l0.2-1-0.405-.4,0.2-.8h0.405l0.2,2.1V28Zm-4.361,13.8,0.2,0.2v0.6l-0.406.4-0.405-.3V42.6c-1.015-1.3-1.522-2.1-1.522-2.6h0.71l1.116,2.2Z" transform="translate(-124 -17)"/></svg>',
					nextArrow: '<svg class="slick-arrow_next" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><path d="M134.21,56.6l0.405-.4h0.507v0.4l-0.507.4Zm8.519-19.7,0.811-.2,0.406,0.6-0.2.6a1.741,1.741,0,0,1-1.014-.8V36.9Zm7.911,7.5a1.378,1.378,0,0,0,.507.9l-0.406-1.5a2.277,2.277,0,0,1-.912.1v0.4l-0.406-.4V43.7a1.688,1.688,0,0,0,1.116-.9l-0.2-.7c0-.1-0.1-0.2-0.1-0.3l-0.405-1.5a0.364,0.364,0,0,1-.1-0.3V39.8h-0.2c0,0.7-.2,1-0.608,1V39.4H148.51c0,0.5-.1.8-0.406,0.8a0.544,0.544,0,0,0,0,.151,0.537,0.537,0,0,0,.609.449l-0.406.4c-0.71,0-1.521-1-2.738-3.2l-0.812-1,0.406-.4a1.622,1.622,0,0,0-1.014-1.6l-0.811.4c0-.4-0.1-0.6-0.406-0.6l-0.811.2V34.8l0.2-1h-0.405v0.4h-0.406l-0.811-.6h-1.015v1.2a1.977,1.977,0,0,1,.62,0,1.923,1.923,0,0,1,1.612,2.2,0.866,0.866,0,0,0-1.015,1h-0.1l-1.014,3.8c0,0.1-.1.2-0.1,0.3l-1.521,6c-1.724,4.6-2.84,7.1-3.245,7.5l-1.42-.2-1.623.2-1.014-.2-1.217.2a0.657,0.657,0,0,0-.812-0.6l-1.217.4h-0.2c-0.3,0-.405-0.2-0.405-0.6l0.608-1.5,0.2,0.2h0.609V52.9h-0.406V52.7c0-.7.608-2.1,1.927-4l0.609-1.4h-0.406V46.8h0.1l0.305,0.3,2.941-6.9H133.2V39.4q1.218,0,1.218-.6l-0.406-1.2,0.406-.8a4.052,4.052,0,0,0-1.218-2.6l-0.2-2.4L133.6,30l-0.2-1.2,0.2-.6h0.406l0.2,0.8-0.1,1.2c0.1,1.2.406,1.8,0.812,1.8h1.42c0.3,0,.709-0.6,1.014-1.8l-0.2-.6,0.406-1.3-0.2-.8a1.093,1.093,0,0,1,.811-0.8l0.609,0.2,0.405-.6-0.2-.2h-1.217V25.7h0.2l0.609,0.2c1.014-1.6,2.028-2.5,3.144-2.6l2.738-.2,2.434,0.2,1.015-.2,0.811,0.8,0.2-.1V22.2h1.014v0.4l-0.406,1.2v0.4l0.2,0.6h-0.2l-0.608.2,3.144,7.7-0.2,1,0.2-.2c0.608,0,1.115,1,1.623,3.2a1.124,1.124,0,0,1-.305.8l0.305,0.2,0.2-.2h0.2l0.406,0.4v0.2h-0.2v0.2l-0.2.2c0-.1-0.1-0.1-0.1,0V37.9l-0.406.4v0.4h0.406l0.2-.2c0.405,0.3,1.724,1.9,2.332,1.8l2.942,6.8,0.2-.2h0.2v0.4h-0.405l0.71,1.4c1.217,2,1.825,3.4,1.825,4.1V53h-0.406v0.6H162.2l0.2-.2,0.608,1.4c0,0.4-.1.6-0.405,0.6h-0.2l-0.913-.3-0.406.3h-0.811v0.2l-1.116-.2-0.71.2-0.1-.1-0.3.1-0.812-.1-0.608.1-0.2-.2-1.217.4H154.19c-1.319-1.6-2.942-5.1-4.97-10.6A2.313,2.313,0,0,1,150.64,44.4Zm-20.893,8.4h-0.3l-0.609.6v0.4h0.913l0.2-.9-0.2-.2v0.1Zm0.406-1.5v1.2h0.406V52.3l-0.2-1h-0.2Zm4.26-11.7-0.7.516a1.842,1.842,0,0,0,.8-0.516h-0.1Zm1.521-1.5h-0.2v0.2l0.2,0.2c0-.1.1-0.1,0.1,0V38Zm2.637-2,1.318-.4,0.2,0.2V35.7l-0.913-.3A0.669,0.669,0,0,0,138.571,36.1ZM134.615,33l-0.1.2v1.2l0.406,0.4a1.089,1.089,0,0,0,.811-0.6V33.6C135.731,33.3,135.325,33.1,134.615,33Zm12.678,4.1V36.9l-0.811-.2-0.406.6,0.2,0.6A1.741,1.741,0,0,0,147.293,37.1Zm3.347,8.8a2.37,2.37,0,0,0,.608,1.4l-0.3,1.3h1.014a0.587,0.587,0,0,0-.1-0.4l-0.507-2.1a0.541,0.541,0,0,0-.3-0.2H150.64Zm8.012-3.9,0.2-.2,0.609,0.8-0.406.4-0.406-.4V42Zm2.739-4.5V37.1l-0.2-.2V36.7L162,36.1l-0.2-.6H162l0.2-.2h0.2l0.608,0.2v0.4a4.007,4.007,0,0,0-1.217,2l-0.608-.2Zm-6.9-17.8h0.2c0.3,0,.507.6,0.811,1.8H155.1l-1.622-.4Zm2.535,0.1,0.2-.2,0.2,0.6v0.2h-0.406l-0.2-.6h0.2ZM144.555,17h0.405v0.4l-0.811,1h-0.2V17.6Zm-3.246,4.4a1.043,1.043,0,0,1-.608-1.2h1.014l-0.2,1.2h-0.2ZM135.528,28H134.92V27.5l0.2-1-0.405-.4,0.2-.8h0.405l0.2,2.1V28Zm-4.361,13.8,0.2,0.2v0.6l-0.406.4-0.405-.3V42.6c-1.015-1.3-1.522-2.1-1.522-2.6h0.71l1.116,2.2Z" transform="translate(-124 -17)"/></svg>',
					variableWidth: true,
					mobileFirst: true,
					responsive: [
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 3,
							}
						},
						{
							breakpoint: 1279,
							settings: {
								slidesToShow: 5,
								variableWidth: false,
							}
						}
					]

				});



		});

	}








	//****************************************//

	$('input[type = tel]').each(function () {
		let inputmask = new Inputmask({
			mask: '+7 (999) 999-99-99',
			showMaskOnHover: false,
			onincomplete: function() {
				this.value = '';
				$(this).closest('.form__field').removeClass('f-filled');
			}
		});
		inputmask.mask($(this)[0]);
	});

	$b
		.on('click', '[data-scroll]', function (e) {
			let $t = $(this),
				hash = $t.attr('data-scroll');

			if ( $(hash).length > 0){
				e.preventDefault();
				$('html,body').animate({scrollTop:$(hash).offset().top + 1}, 500);
			}
		})

		.on('blur', '.form__field input, .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('submit', '.form', function (e) {
			let $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});

			if ( wrong ){
				e.preventDefault();
			}
		})
	;




	ymaps.ready(function(){



		$('.map').each(function () {
			let $t = $(this),
				center = $t.attr('data-center') ? JSON.parse($t.attr('data-center')) : [0, 0],
				poly = $t.attr('data-poly') ? JSON.parse($t.attr('data-poly')) : [],
				zoom = $t.attr('data-zoom') || 15,
				marker = $t.attr('data-marker') ? JSON.parse($t.attr('data-marker')) : false
			;

			let map = new ymaps.Map($t[0],{
					center: center,
					zoom: zoom,
					controls: []
				});

			map.controls.add('zoomControl', {
				size: 'small',
				float: 'none',
				position: {
					top: '250px',
					left: '30px'
				}
			});

			if ( poly.length ){
				let myGeoObject = new ymaps.GeoObject({
					geometry: {
						type: "Polygon",
						coordinates: [poly],
						// Задаем правило заливки внутренних контуров по алгоритму "nonZero".
						fillRule: "nonZero"
					},
					// Описываем свойства геообъекта.
					// properties:{
					// 	balloonContent: "Многоугольник"
					// }
				}, {
					// Описываем опции геообъекта.
					// Цвет заливки.
					fillColor: '#FF6600',
					// Цвет обводки.
					strokeColor: '#FF6600',
					// Общая прозрачность (как для заливки, так и для обводки).
					fillOpacity: 0.5,
					// Ширина обводки.
					strokeWidth: 5,
					strokeOpacity: 0,
					// Стиль обводки.
					strokeStyle: 'solid'
				});

				map.geoObjects.add(myGeoObject);
			}


			if ( marker ){
				let myPlacemark = new ymaps.Placemark(marker,
					{},
					{
						iconLayout: 'default#image',
						iconImageHref: '/img/svg/dot.svg',
						iconImageSize: [48, 65],
						iconImageOffset: [-24, -65]
					});
				map.geoObjects.add(myPlacemark);
			}

			map.behaviors.disable('scrollZoom');
			map.behaviors.disable('multiTouch');
			map.behaviors.disable('drag');

			let timer = null;
			$(window).on('resize', ()=>{

				clearTimeout(timer);
				timer = setTimeout(()=>{
					map.container.fitToViewport();
				}, 200)

			})

		});


	});
});

