
/* ===========================================================
 * Simple Full Page Scroll beta
 * ===========================================================
 * Copyright 2015 Matheus Verissimo.
 * ma.theus_verissimo@hotmail.com
 *
 * Create awesome pages full scroll
 *
 * ========================================================== */

'use strict';

export default class FullPage {
	constructor(element, options) {

		let defaults = {
			section: '.section',

			animationDuration: 700,
			animationTiming: 'ease',
			animationTranform: 'transform',

			pagination: true,
			keyboard: true,

			touch: true,
			touchLimit: 100,

			loop: false,

			onLeave: null,
			afterLoad: null,

		};


		console.info(this.utils());

		// Element
		this.el = document.querySelector(element);

		// Settings
		this.settings = this.utils().extend(defaults, options);

		// Body
		this.body = document.querySelector('body');

		// Sections
		this.sections = this.el.querySelectorAll(this.settings.section);

		//init
		this.init();

		return this;
	}

	utils() {
		return {
			// From http://stackoverflow.com/questions/11197247/javascript-equivalent-of-jquerys-extend-method
			extend: function(defaults, options) {
				if (typeof(options) !== 'object') {
					options = {};
				}

				for (let key in options) {
					if (defaults.hasOwnProperty(key)) {
						defaults[key] = options[key];
					}
				}

				return defaults;

			},

			// From http://stackoverflow.com/questions/10964966/detect-ie-version-prior-to-v9-in-javascript
			isIE: function() {
				let myNav = navigator.userAgent.toLowerCase();
				return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
			},

			setStyle: function(el, property, value) {
				el.style[property.charAt(0).toLowerCase() + property.slice(1)] = value;

			},

			setVendor: function(el, property, value) {
				el.style[property.charAt(0).toLowerCase() + property.slice(1)] = value;
				el.style['webkit' + property] = value;
				el.style['moz' + property] = value;
				el.style['ms' + property] = value;
				el.style['o' + property] = value;

			},

			// From http://jaketrent.com/post/addremove-classes-raw-javascript/
			hasClass: function(el, className) {
				if (el.classList) {
					return el.classList.contains(className);
				} else {
					return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
				}

			},

			addClass: function(el, className) {

				console.info(el);

				if (el.classList) {
					el.classList.add(className);
				} else if (!this.utils().hasClass(el, className)) {
					el.className += " " + className;
				}
			},

			removeClass: function(el, className) {
				if (el.classList)
					el.classList.remove(className)
				else if (this.utils().hasClass(el, className)) {
					let reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
					el.className = el.className.replace(reg, ' ');
				}

			},

		}
	}

	init (){

		this.index = 0;
		this.lastAnimation = 0;

		this.build();
		this.bindEvents();
		this.makeActive(this.index);

		if (typeof this.settings.afterLoad === 'function') {
			this.settings.afterLoad(this.index);
		}


	};

	build (){
		if (this.settings.pagination) {
			this.paginationHTML();

		}

	};

	bindEvents (){
		let self = this;

		if (this.settings.pagination) {
			let paginationLinks = document.querySelectorAll('.slide-navigation li a');

			for (let i = 0; i < paginationLinks.length; i++) {

				(function(index) {

					paginationLinks[i].addEventListener('click', function(event) {
						self.index = index;
						self.move(self.index);

						event.preventDefault();

					});

				})(i);

			}

		}

		if (this.settings.keyboard) {
			document.addEventListener('keydown', this.keyboard.bind(this));

		}

		if (this.settings.touch) {
			this.enableTouch(document);

		}

		document.addEventListener('mousewheel', this.mousewheel.bind(this));
		document.addEventListener('DOMMouseScroll', this.mousewheel.bind(this));

	};

	makeActive (index){
		let self = this,
			paginationLinks = document.querySelectorAll('.slide-navigation li a');

		for (let i = 0; i < this.sections.length; i++) {
			this.utils().removeClass(this.sections[i], 'is-active');
			this.utils().removeClass(paginationLinks[i], 'is-active');
		}

		console.info(this.sections, index);
		this.utils().addClass(this.sections[index], 'is-active');
		this.utils().addClass(paginationLinks[index], 'is-active');

	};

	move (index){

		let self = this;

		if (typeof self.settings.onLeave === 'function') {
			self.settings.onLeave(this.index);

		}

		if (this.utils().isIE() === 9) {
			this.utils().setStyle(this.el, 'position', 'relative');
			this.utils().setStyle(this.el, 'top', index * -100 + '%');
			// this.utils().setVendor(this.el, 'Transform', 'translate3d(0, ' + index * -100 + '%, 0)');
			this.utils().setVendor(this.el, 'Transition', 'transform ' + this.settings.animationDuration + 'ms');

		}

		this.utils().setVendor(this.el, 'Transform', 'translate3d(0, ' + index * -100 + '%, 0)');
		this.utils().setVendor(this.el, 'Transition', 'transform ' + this.settings.animationDuration + 'ms');

		let checkEnd = ()=>{
			if (typeof self.settings.afterLoad === 'function') {
				self.settings.afterLoad(self.index);
			}
		};

		this.el.addEventListener('transitionend', checkEnd);

		this.index = index;

		this.makeActive(index);

	}

	moveUp (){
		if (this.index > 0) {
			this.move(this.index - 1);

		}

	}

	moveDown (){
		if ((this.index + 1) < this.sections.length) {
			this.move(this.index + 1);

		}
	}

	moveTo (index){
		this.move(index);

	}

	enableTouch (el){
		let self = this,
			startCoords = 0,
			endCoords = 0,
			distance = 0;

		el.addEventListener('touchstart', function(event) {
			startCoords = event.changedTouches[0].pageY;

		});

		el.addEventListener('touchmove', function(event) {
			event.preventDefault();

		});

		el.addEventListener('touchend', function(event) {
			let time = new Date().getTime();

			endCoords = event.changedTouches[0].pageY;
			distance = endCoords - startCoords;

			if (time - Math.abs(self.lastAnimation) < self.settings.animationDuration) {
				return;

			}

			if ((distance < 0) && (Math.abs(distance) > self.settings.touchLimit)) {
				self.moveDown();

			} else if ((distance > 0) && (Math.abs(distance) > self.settings.touchLimit)) {
				self.moveUp();

			}

			self.lastAnimation = time;

		});

	}

	mousewheel (event){
		let time = new Date().getTime();
		let delta = event.wheelDelta || -event.detail;

		if (time - Math.abs(this.lastAnimation) < this.settings.animationDuration) {
			return;

		}

		if (delta < 0) {
			this.moveDown();

		} else {
			this.moveUp();

		}

		this.lastAnimation = time;

	}

	keyboard (event){

		let time = new Date().getTime();

		if (time - Math.abs(this.lastAnimation) < this.settings.animationDuration) {
			return;

		}

		if (event.keyCode === 38) {
			this.moveUp();

		}

		if (event.keyCode === 40) {
			this.moveDown();

		}

		this.lastAnimation = time;

	}

	paginationHTML (){
		let paginationList = '';

		for (let i = 0; i < this.sections.length; i++) {
			paginationList += '<li><a data-index=\"' + i + '\" href=\"#' + i + '"\></a></li>';

		}

		let pagination = document.createElement('ul');
		pagination.setAttribute('class', 'slide-navigation');
		pagination.innerHTML = paginationList;

		this.body.appendChild(pagination);

	}

}
